package com.ml.base.injection.components
import com.ml.home.R

import com.ml.base.injection.modules.LoginModule
import dagger.BindsInstance
import dagger.Component

@Component(
    modules = [
        LoginModule::class
    ]
)
interface BaseComponent {
    @Component.Builder
    interface Builder {
        fun build(): BaseComponent
    }
}