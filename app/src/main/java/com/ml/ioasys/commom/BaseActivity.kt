package com.ml.ioasys.commom

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.ml.ioasys.R
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Function


open class BaseActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setToolbar()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setBackgroundDrawable(
            ContextCompat.getDrawable(
                BaseActivity@ this,
                R.drawable.gradient_toolbar
            )
        )
    }

    // EXTENSIONS ----------------------------------------------------------------------------------
    inline fun <T> subscribeMainThread(crossinline uiAction: T.() -> Unit):
            Function<Observable<T>, Disposable> = subscribeMainThread(
        done = {},
        next = uiAction,
        error = {}
    )

    inline fun <T> subscribeMainThread(
        crossinline done: (() -> Unit),
        crossinline error: Throwable.() -> Unit,
        crossinline next: ((T) -> Unit)
    ): Function<Observable<T>, Disposable> = Function { observable ->
        observable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { next.invoke(it) },
                {
                    error.invoke(it)
                },
                { done.invoke() }
            )
    }

    fun View.hideKeyboard() {
        var inputMethodManager= getSystemService(Context.INPUT_METHOD_SERVICE)
                as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    fun View.showKeyboard(){
        var inputMethodManager= getSystemService(Context.INPUT_METHOD_SERVICE)
                as InputMethodManager
        inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }
}