package com.ml.ioasys.commom.rx

import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class ReactivePresenter<RVI> {

    private val disposable = CompositeDisposable()
    private var view: RVI? = null
    val isBinded: Boolean
        get() = view != null

    open fun bind(view: RVI) {
        this.view = view
    }

    open fun unbind() {
        disposable.clear()
        this.view = null
    }

    fun getDisposable(): CompositeDisposable {
        return disposable
    }

    protected fun view(): RVI? {
        return view
    }
}