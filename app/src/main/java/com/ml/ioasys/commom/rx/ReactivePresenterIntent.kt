package com.ml.ioasys.commom.rx

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Function

abstract class ReactivePresenterIntent<PS : PartialState, SVM : StateViewModel<PS, SVM>,
        RVI : ReactiveViewIntent<SVM>> : ReactivePresenter<RVI>() {

    protected fun subscribeAllActions(vararg actions: Observable<PS>?) {
        view()?.apply {
            val observable = mergeActions(actions.toList(), initialState)
            getDisposable().clear()
            observable.runOnSignal(render())?.let { getDisposable().add(it) }
        }
    }

    private fun mergeActions(actions: List<Observable<PS>?>, initialState: SVM): Observable<SVM> {
        return Observable.merge(actions)
            .scan(initialState) { state, partialState -> state.reducer(partialState) }
            .skip(1)
            .distinctUntilChanged()
    }

    private fun <T> Observable<T>.runOnSignal(uiFunc: Function<Observable<T>, Disposable>?): Disposable? =
        try {
            uiFunc?.apply(this)
        } catch (e: Exception) {
            null
        }
}