package com.ml.ioasys.commom.util

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TextView
import java.util.*


class SearchTextWatcher {
    companion object{
        fun build(callback: () -> Unit) : TextWatcher {
            var searchText: EditText
            var resultText: TextView
            var timer = Timer()

            return object : TextWatcher {
                override fun afterTextChanged(arg0: Editable) { // user typed: start the timer
                    timer = Timer()
                    timer.schedule(object : TimerTask() {
                        override fun run() {
                            callback()
                        }
                    }, 600) // 600ms delay before the timer executes the „run“ method from TimerTask
                }

                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) { // nothing to do here
                }

                override fun onTextChanged(
                    s: CharSequence,
                    start: Int,
                    before: Int,
                    count: Int
                ) { // user is typing: reset already started timer (if existing)
                    timer?.cancel()
                }
            }
        }
    }
}