package com.ml.ioasys.commom.rx

interface StateViewModel<in PS, out SVM> {
    fun reducer(partialState: PS) : SVM
}
