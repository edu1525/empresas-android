package com.ml.ioasys.commom.rx

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Function

interface ReactiveViewIntent<SVM> {
    val initialState : SVM

    fun render() : Function<Observable<SVM>, Disposable>
}
