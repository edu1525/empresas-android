package com.ml.ioasys.injection.modules.android

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.ml.ioasys.R
import dagger.Module
import dagger.Provides

@Module
class AppModule {
    @Provides
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }

    @Provides
    fun provideSharedPreferences(context: Context) : SharedPreferences {
        return context.getSharedPreferences(context.getString(R.string.preferences_name),
            Context.MODE_PRIVATE)
    }
}