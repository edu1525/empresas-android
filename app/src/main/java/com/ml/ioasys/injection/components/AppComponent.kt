package com.ml.ioasys.injection.components

import android.app.Application
import com.ml.ioasys.injection.modules.android.ActivityModule
import com.ml.ioasys.commom.application.IoAsysApplication
import com.ml.ioasys.injection.modules.android.AppModule
import com.ml.ioasys.injection.modules.android.RetrofitModule
import com.ml.ioasys.injection.modules.features.HomeModule
import com.ml.ioasys.injection.modules.features.LoginModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ActivityModule::class,
        AndroidInjectionModule::class,
        AppModule::class,
        RetrofitModule::class,
        LoginModule::class,
        HomeModule::class
    ]
)
interface AppComponent : AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: IoAsysApplication)
}