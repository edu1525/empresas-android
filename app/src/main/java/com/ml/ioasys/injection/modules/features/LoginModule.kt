package com.ml.ioasys.injection.modules.features

import com.ml.ioasys.features.login.domain.LoginSource
import com.ml.ioasys.features.login.infrastructure.LoginInfrastructure
import com.ml.ioasys.features.login.infrastructure.mapper.LoginMapper
import com.ml.ioasys.features.login.network.LoginApi
import com.ml.ioasys.features.login.presentation.LoginPresenter
import com.ml.ioasys.features.login.presentation.mapper.LoginViewModelMapper
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class LoginModule {

    @Provides
    fun provideLoginApi(retrofit: Retrofit): LoginApi = retrofit.create(LoginApi::class.java)

    @Provides
    fun provideLoginInfrastructure(loginApi: LoginApi): LoginSource =
        LoginInfrastructure(loginApi, Schedulers.io(), LoginMapper())


    @Provides
    fun provideLoginPresenter(loginSource: LoginSource): LoginPresenter =
        LoginPresenter(loginSource, LoginViewModelMapper())

}