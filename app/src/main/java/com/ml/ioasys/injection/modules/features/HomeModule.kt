package com.ml.ioasys.injection.modules.features

import com.ml.ioasys.features.home.domain.HomeSource
import com.ml.ioasys.features.home.infrastructure.HomeInfrastructure
import com.ml.ioasys.features.home.infrastructure.mapper.HomeMapper
import com.ml.ioasys.features.home.network.HomeApi
import com.ml.ioasys.features.home.presentation.HomePresenter
import com.ml.ioasys.features.home.presentation.mapper.HomeViewModelMapper
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Retrofit

@Module
class HomeModule {
    @Provides
    fun provideHomeApi(retrofit: Retrofit): HomeApi =
        retrofit.create(HomeApi::class.java)


    @Provides
    fun provideHomeSource(homeApi: HomeApi): HomeSource =
        HomeInfrastructure(homeApi, Schedulers.io(), HomeMapper())


    @Provides
    fun provideHomePresenter(homeSource: HomeSource): HomePresenter =
        HomePresenter(homeSource, HomeViewModelMapper())

}