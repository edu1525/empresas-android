package com.ml.ioasys.injection.modules.android

import com.ml.ioasys.features.home.ui.HomeActivity
import com.ml.ioasys.features.login.ui.LoginActivity
import com.ml.ioasys.features.splash.ui.SplashActivity
import com.ml.ioasys.injection.modules.features.HomeModule
import com.ml.ioasys.injection.modules.features.LoginModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeSplashActivity() : SplashActivity

    @ContributesAndroidInjector(modules = [LoginModule::class])
    abstract fun contributeLoginActivity() : LoginActivity

    @ContributesAndroidInjector(modules = [HomeModule::class])
    abstract fun contributeHomeActivity() : HomeActivity
}