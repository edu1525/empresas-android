package com.ml.ioasys.features.home.infrastructure.mapper

import com.ml.ioasys.features.home.domain.model.Company
import com.ml.ioasys.features.home.domain.model.HomeResult
import com.ml.ioasys.features.home.infrastructure.model.CompanyPayload
import com.ml.ioasys.features.home.infrastructure.model.HomePayload

class HomeMapper {
    fun map(homePayload: HomePayload): HomeResult {
        val companies = homePayload.companies.map { mapCompany(it) }
        return HomeResult(companies)
    }

    private fun mapCompany(company: CompanyPayload): Company {
        return Company(
            company.id,
            company.name,
            company.photo,
            company.description,
            company.country
        )
    }
}