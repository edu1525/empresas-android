package com.ml.ioasys.features.login.infrastructure.model

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("investor")
    val investor: InvestorResponse
)

data class InvestorResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("investor_name")
    val name: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("city")
    val city: String,
    @SerializedName("country")
    val country: String,
    @SerializedName("photo")
    val photo: String
)