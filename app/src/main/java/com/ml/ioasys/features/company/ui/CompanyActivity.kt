package com.ml.ioasys.features.company.ui

import android.os.Bundle
import com.bumptech.glide.Glide
import com.ml.ioasys.commom.BaseActivity
import com.ml.ioasys.R
import com.ml.ioasys.features.home.presentation.model.CompanyViewModel
import kotlinx.android.synthetic.main.activity_company.*

class CompanyActivity : BaseActivity() {
    private lateinit var company: CompanyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company)

        company = intent.extras?.getSerializable("company") as CompanyViewModel

        if(company != null) {
            setToolbar()
            setView()
        }
    }

    private fun setToolbar(){
        supportActionBar?.title = company.name
    }

    private fun setView(){
        tv_placeholder.text = company.name?.first().toString()
        tv_description.text = company.description

        if(company.photo != null){
            Glide.with(this).load(company.photo).into(img_company);
        }
    }
}
