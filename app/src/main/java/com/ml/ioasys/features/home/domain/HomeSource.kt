package com.ml.ioasys.features.home.domain

import com.ml.ioasys.features.home.domain.model.HomeResult
import io.reactivex.rxjava3.core.Observable

interface HomeSource {
    fun filterCompanies(query: String) : Observable<HomeResult>
}