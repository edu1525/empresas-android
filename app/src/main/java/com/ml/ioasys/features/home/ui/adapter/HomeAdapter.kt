package com.ml.ioasys.features.home.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ml.ioasys.R
import com.ml.ioasys.features.company.ui.CompanyActivity
import com.ml.ioasys.features.home.presentation.model.CompanyViewModel
import com.ml.ioasys.features.home.ui.adapter.holder.HomeViewHolder
import kotlinx.android.synthetic.main.item_list_home.view.*

class HomeAdapter(
    val context : Context,
    private var items : List<CompanyViewModel>
) : RecyclerView.Adapter<HomeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_list_home, parent, false)
        return HomeViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val item = items[position]
        holder.itemView.tv_placeholder.text = item.name?.first().toString()
        holder.itemView.tv_name.text = item.name
        holder.itemView.tv_country.text = item.country

        if(item.photo != null){
            Glide.with(context).load(item.photo).into(holder.itemView.img_company);
        }
        holder.itemView.ll_company.setOnClickListener {
            val intent = Intent(context, CompanyActivity::class.java)
            intent.putExtra("company", item)
            context.startActivity(intent)
        }
    }

    fun setData(items: List<CompanyViewModel>) {
        this.items = items
        notifyDataSetChanged()
    }
}