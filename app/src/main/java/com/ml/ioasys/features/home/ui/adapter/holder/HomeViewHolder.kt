package com.ml.ioasys.features.home.ui.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class HomeViewHolder(view: View) : RecyclerView.ViewHolder(view)