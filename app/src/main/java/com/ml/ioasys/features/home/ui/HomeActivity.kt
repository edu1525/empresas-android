package com.ml.ioasys.features.home.ui

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.ml.ioasys.commom.BaseActivity
import com.ml.ioasys.R
import com.ml.ioasys.commom.util.SearchTextWatcher
import com.ml.ioasys.features.home.presentation.HomePresenter
import com.ml.ioasys.features.home.presentation.HomeView
import com.ml.ioasys.features.home.presentation.model.state.HomeState
import com.ml.ioasys.features.home.ui.adapter.HomeAdapter
import com.ml.ioasys.features.login.ui.LoginActivity
import dagger.android.AndroidInjection
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Function
import io.reactivex.rxjava3.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_home.*
import retrofit2.HttpException
import javax.inject.Inject

class HomeActivity : BaseActivity(), HomeView {
    private val filterCompaniesAction = PublishSubject.create<String>()
    override fun filterCompaniesIntent(): Observable<String> = filterCompaniesAction

    @Inject
    lateinit var homePresenter: HomePresenter

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    private var showSearch = false

    private val homeAdapter: HomeAdapter by lazy {
        HomeAdapter(HomeActivity@ this, arrayListOf())
    }

    override val initialState: HomeState
        get() = HomeState()

    override fun render(): Function<Observable<HomeState>, Disposable> {
        return subscribeMainThread<HomeState> {
            showLoading(this)
            showData(this)
            handleError(this)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        homePresenter.bind(this)

        setToolbar()
        setView()
    }

    override fun onBackPressed() {
        if (showSearch) {
            showOrHideSearch()
        } else {
            super.onBackPressed()
        }
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
    }

    private fun setView() {
        bt_search.setOnClickListener { showOrHideSearch() }
        bt_close_search.setOnClickListener { showOrHideSearch() }
        et_search.addTextChangedListener(SearchTextWatcher.build {
            filterCompaniesAction.onNext(et_search.text.toString())
        })
        list_companies.layoutManager = LinearLayoutManager(HomeActivity@ this)
        list_companies.adapter = homeAdapter
    }

    private fun showOrHideSearch() {
        showSearch = !showSearch
        if (showSearch) {
            rl_default.visibility = View.GONE
            ll_search.visibility = View.VISIBLE
            et_search.requestFocus()
            et_search.showKeyboard()
        } else {
            rl_default.visibility = View.VISIBLE
            ll_search.visibility = View.GONE
            et_search.clearFocus()
            et_search.text = null
            et_search.hideKeyboard()
        }
    }

    private fun showLoading(state: HomeState) {
        if (state.loading) {
            progress_search.visibility = View.VISIBLE
        } else {
            progress_search.visibility = View.GONE
        }
    }

    private fun showData(state: HomeState) {
        if (state.data != null) {
            if (!state.data.companies.isNullOrEmpty()) {
                list_companies.visibility = View.VISIBLE
                tv_search_tip.visibility = View.GONE
                tv_companies_not_found.visibility = View.GONE
                homeAdapter.setData(state.data.companies)
            } else {
                list_companies.visibility = View.GONE
                tv_search_tip.visibility = View.GONE
                tv_companies_not_found.visibility = View.VISIBLE
            }
        }
    }

    private fun handleError(state: HomeState) {
        if (state.error != null) {
            when (state.error) {
                is HttpException -> {
                    if (state.error.code() == 401)
                        logout()
                    else
                        showUnexpectedError()
                }
                else -> showUnexpectedError()
            }
        }
    }

    private fun logout() {
        Toast.makeText(HomeActivity@ this, getString(R.string.expired_session), Toast.LENGTH_LONG)
            .show()
        sharedPreferences.edit().clear().commit()
        startActivity(Intent(HomeActivity@ this, LoginActivity::class.java))
        finish()

    }

    private fun showUnexpectedError() {
        Toast.makeText(HomeActivity@ this, getString(R.string.unexpected_error), Toast.LENGTH_LONG)
            .show()
    }
}
