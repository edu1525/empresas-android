package com.ml.ioasys.features.home.presentation

import com.ml.ioasys.commom.rx.ReactivePresenterIntent
import com.ml.ioasys.features.home.domain.HomeSource
import com.ml.ioasys.features.home.presentation.mapper.HomeViewModelMapper
import com.ml.ioasys.features.home.presentation.model.state.HomePartialState
import com.ml.ioasys.features.home.presentation.model.state.HomeState
import io.reactivex.rxjava3.core.Observable

class HomePresenter(
    private val homeSource: HomeSource,
    private val mapper: HomeViewModelMapper
) : ReactivePresenterIntent<HomePartialState, HomeState, HomeView>() {

    override fun bind(view: HomeView) {
        super.bind(view)

        val filterCompaniesAction : Observable<HomePartialState> = filterCompaniesCall()
        subscribeAllActions(filterCompaniesAction)
    }

    private fun filterCompaniesCall() : Observable<HomePartialState> {
        return view()
            ?.filterCompaniesIntent()!!
            .flatMap {
                filterCompanies(it)
                    .map<HomePartialState>(HomePartialState::HomeLoaded)
                    .onErrorReturn(HomePartialState::HomeError)
                    .startWith(Observable.just(HomePartialState.HomeLoading))

            }
    }

    private fun filterCompanies(query: String) =
        homeSource.filterCompanies(query)
            .map(mapper::map)

}