package com.ml.ioasys.features.login.domain.model

data class LoginResult(
    val accessToken: String,
    val client: String,
    val uid: String,
    val investor: Investor?
)

data class Investor(
    val id: String,
    val name: String,
    val email: String,
    val city: String,
    val country: String,
    val photo: String
)