package com.ml.ioasys.features.home.infrastructure

import com.ml.ioasys.features.home.domain.HomeSource
import com.ml.ioasys.features.home.domain.model.HomeResult
import com.ml.ioasys.features.home.infrastructure.mapper.HomeMapper
import com.ml.ioasys.features.home.network.HomeApi
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler

class HomeInfrastructure(
    val homeApi: HomeApi,
    val ioScheduler: Scheduler,
    val homeMapper: HomeMapper
) : HomeSource {

    override fun filterCompanies(query: String): Observable<HomeResult> {
        return homeApi.filterCompanies(query)
            .subscribeOn(ioScheduler)
            .map(homeMapper::map)
    }
}