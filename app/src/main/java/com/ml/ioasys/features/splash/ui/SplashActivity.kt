package com.ml.ioasys.features.splash.ui

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.ml.ioasys.R
import com.ml.ioasys.features.home.ui.HomeActivity
import com.ml.ioasys.features.login.ui.LoginActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({ checkAuth() }, 2000)
    }

    private fun checkAuth() {
        if (sharedPreferences.getString("accessToken", null) != null) {
            startActivity(Intent(SplashActivity@ this, HomeActivity::class.java))
        } else {
            startActivity(Intent(SplashActivity@ this, LoginActivity::class.java))
        }
        finish()
    }
}
