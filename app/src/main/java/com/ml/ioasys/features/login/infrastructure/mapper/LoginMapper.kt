package com.ml.ioasys.features.login.infrastructure.mapper

import com.ml.ioasys.features.login.domain.model.Investor
import com.ml.ioasys.features.login.domain.model.LoginResult
import com.ml.ioasys.features.login.infrastructure.model.InvestorResponse
import com.ml.ioasys.features.login.infrastructure.model.LoginResponse
import retrofit2.Response

class LoginMapper {
    fun map(loginResponse: Response<LoginResponse>): LoginResult? {
        val headers = loginResponse.headers()
        val accessToken = headers.get("access-token")
        val client = headers.get("client")
        val uid = headers.get("uid")

        return if (accessToken != null && client != null && uid != null) {
            var investor = loginResponse.body()?.let { mapInvestor(it.investor) }
            LoginResult(accessToken, client, uid, investor)
        } else null
    }

    private fun mapInvestor(investorResponse: InvestorResponse): Investor {
        return Investor(
            investorResponse.id,
            investorResponse.name,
            investorResponse.email,
            investorResponse.city,
            investorResponse.country,
            investorResponse.photo
        )
    }
}