package com.ml.ioasys.features.home.domain.model

data class HomeResult(
    val companies: List<Company> = arrayListOf()
)

data class Company(
    val id: Int,
    val name: String?,
    val photo: String?,
    val description: String?,
    val country: String?
)