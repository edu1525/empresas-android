package com.ml.ioasys.features.login.presentation.model.state

import com.ml.ioasys.commom.rx.StateViewModel
import com.ml.ioasys.features.login.presentation.model.LoginViewModel

data class LoginState(
    val loading: Boolean = false,
    val data: LoginViewModel? = null,
    val error : Throwable? = null) : StateViewModel<LoginPartialState, LoginState> {

    override fun reducer(partialState: LoginPartialState): LoginState {
        val oldState = this

        return when(partialState){
            is LoginPartialState.Loading -> oldState.copy(
                loading = true,
                data = null,
                error =  null
            )
            is LoginPartialState.LoggedIn -> oldState.copy(
                loading = false,
                data =  partialState.data,
                error = null
            )
            is LoginPartialState.LoginError -> oldState.copy(
                loading = false,
                data = null,
                error = partialState.throwable
            )
        }
    }
}