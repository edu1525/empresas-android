package com.ml.ioasys.features.login.presentation.mapper

import com.ml.ioasys.features.login.domain.model.LoginResult
import com.ml.ioasys.features.login.presentation.model.LoginViewModel

class LoginViewModelMapper {

    fun map(loginResult: LoginResult?): LoginViewModel {
        val name = loginResult?.investor?.let { it.name }
        val accessToken = loginResult?.let { it.accessToken }
        val client = loginResult?.let { it.client }
        val uid = loginResult?.let { it.uid }
        return LoginViewModel(name, accessToken, client, uid)
    }
}