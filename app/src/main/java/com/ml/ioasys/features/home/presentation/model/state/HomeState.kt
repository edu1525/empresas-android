package com.ml.ioasys.features.home.presentation.model.state

import com.ml.ioasys.commom.rx.StateViewModel
import com.ml.ioasys.features.home.presentation.model.HomeViewModel

data class HomeState(
    val loading: Boolean = false,
    val data: HomeViewModel? = null,
    val error: Throwable? = null
) : StateViewModel<HomePartialState, HomeState> {
    override fun reducer(partialState: HomePartialState): HomeState {
        val oldState = this
        return when (partialState) {
            is HomePartialState.HomeLoading -> oldState.copy(
                loading = true,
                data = null,
                error = null
            )
            is HomePartialState.HomeLoaded -> oldState.copy(
                loading = false,
                data = partialState.data,
                error = null
            )
            is HomePartialState.HomeError -> oldState.copy(
                loading = false,
                data = null,
                error = partialState.throwable
            )
        }
    }
}