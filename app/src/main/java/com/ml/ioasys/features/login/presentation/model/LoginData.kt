package com.ml.ioasys.features.login.presentation.model

data class LoginData(
    val email: String,
    val password: String
)