package com.ml.ioasys.features.home.presentation.model

import java.io.Serializable

data class HomeViewModel(
    val companies: List<CompanyViewModel> = arrayListOf()
)

data class CompanyViewModel(
    val id: Int,
    val name: String?,
    val photo: String?,
    val description: String?,
    val country: String?
) : Serializable