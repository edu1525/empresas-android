package com.ml.ioasys.features.home.presentation

import com.ml.ioasys.commom.rx.ReactiveViewIntent
import com.ml.ioasys.features.home.presentation.model.state.HomeState
import io.reactivex.rxjava3.core.Observable

interface HomeView : ReactiveViewIntent<HomeState> {
    fun filterCompaniesIntent() : Observable<String>
}