package com.ml.ioasys.features.login.presentation

import com.ml.ioasys.commom.rx.ReactiveViewIntent
import com.ml.ioasys.features.login.presentation.model.LoginData
import com.ml.ioasys.features.login.presentation.model.state.LoginState
import io.reactivex.rxjava3.core.Observable

interface LoginView : ReactiveViewIntent<LoginState> {

    fun makeLoginIntent(): Observable<LoginData>
}