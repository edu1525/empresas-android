package com.ml.ioasys.features.login.presentation.model.state

import com.ml.ioasys.commom.rx.PartialState
import com.ml.ioasys.features.login.presentation.model.LoginViewModel

sealed class LoginPartialState : PartialState{
    object Loading : LoginPartialState()
    data class LoggedIn(val data: LoginViewModel) : LoginPartialState()
    data class LoginError(val throwable: Throwable) : LoginPartialState()
}