package com.ml.ioasys.features.home.infrastructure.model

import com.google.gson.annotations.SerializedName

data class HomePayload(
    @SerializedName("enterprises")
    val companies: List<CompanyPayload>
)

data class CompanyPayload(
    @SerializedName("id")
    val id: Int,
    @SerializedName("enterprise_name")
    val name: String?,
    @SerializedName("photo")
    val photo: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("country")
    val country: String?
)