package com.ml.ioasys.features.login.network

import com.ml.ioasys.features.login.infrastructure.model.LoginPayload
import com.ml.ioasys.features.login.infrastructure.model.LoginResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi {
    @POST("users/auth/sign_in")
    fun makeLogin(@Body credentials: LoginPayload) : Observable<Response<LoginResponse>>
}