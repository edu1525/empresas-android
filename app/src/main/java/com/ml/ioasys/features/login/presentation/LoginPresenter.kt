package com.ml.ioasys.features.login.presentation

import com.ml.ioasys.commom.rx.ReactivePresenterIntent
import com.ml.ioasys.features.login.domain.LoginSource
import com.ml.ioasys.features.login.presentation.mapper.LoginViewModelMapper
import com.ml.ioasys.features.login.presentation.model.state.LoginPartialState
import com.ml.ioasys.features.login.presentation.model.state.LoginState
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableSource
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.core.SingleSource

class LoginPresenter(
    val loginSouce: LoginSource,
    val mapper: LoginViewModelMapper
) : ReactivePresenterIntent<LoginPartialState, LoginState, LoginView>() {

    override fun bind(view: LoginView) {
        super.bind(view)

        val makeLoginAction: Observable<LoginPartialState> = makeLoginCall()
        subscribeAllActions(makeLoginAction)
    }

    private fun makeLoginCall(): Observable<LoginPartialState> {
        return view()
            ?.makeLoginIntent()!!
            .flatMap {
                makeLogin(it.email, it.password)
                    .map<LoginPartialState>(LoginPartialState::LoggedIn)
                    .onErrorReturn(LoginPartialState::LoginError)
                    .startWith(Observable.just(LoginPartialState.Loading))
            }
    }

    private fun makeLogin(email: String, password: String) =
        loginSouce.makeLogin(email, password)
            .map(mapper::map)

}