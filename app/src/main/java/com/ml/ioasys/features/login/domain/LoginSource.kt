package com.ml.ioasys.features.login.domain

import com.ml.ioasys.features.login.domain.model.LoginResult
import io.reactivex.rxjava3.core.Observable

interface LoginSource {
    fun makeLogin(email: String, password: String) : Observable<LoginResult?>
}