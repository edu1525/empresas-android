package com.ml.ioasys.features.login.ui

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import com.google.android.material.textfield.TextInputLayout
import com.ml.ioasys.commom.BaseActivity
import com.ml.ioasys.features.home.ui.HomeActivity
import com.ml.ioasys.R
import com.ml.ioasys.commom.util.Validators
import com.ml.ioasys.features.login.presentation.LoginPresenter
import com.ml.ioasys.features.login.presentation.LoginView
import com.ml.ioasys.features.login.presentation.model.LoginData
import com.ml.ioasys.features.login.presentation.model.state.LoginState
import dagger.android.AndroidInjection
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Function
import io.reactivex.rxjava3.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.progress_overlay.*
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginView {
    private val makeLoginAction = PublishSubject.create<LoginData>()

    @Inject
    lateinit var loginPresenter: LoginPresenter

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    override fun makeLoginIntent(): Observable<LoginData> = makeLoginAction

    override val initialState: LoginState = LoginState()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginPresenter.bind(this)

        setView()
    }

    override fun render(): Function<Observable<LoginState>, Disposable> {
        return subscribeMainThread {
            showLoading(this)
            handleData(this)
            handleError(this)
        }
    }

    private fun showLoading(state: LoginState) {
        if (state.loading) {
            progress_overlay.visibility = View.VISIBLE
        } else {
            progress_overlay.visibility = View.GONE
        }
    }

    private fun handleData(state: LoginState) {
        if (state.data != null) {
            Toast.makeText(
                LoginActivity@ this, getString(R.string.welcome) + " " +
                        state.data.investorName, Toast.LENGTH_LONG
            ).show()
            sharedPreferences
                .edit()
                .putString("accessToken", state.data.accessToken)
                .putString("client", state.data.client)
                .putString("uid", state.data.uid)
                .commit()
            startActivity(Intent(LoginActivity@ this, HomeActivity::class.java))
            finish()
        }
    }

    private fun handleError(state: LoginState) {
        if (state.error != null) {
            showEmailError(false)
            showPasswordError()
        }
    }

    private fun setView() {
        bt_enter.setOnClickListener { makeLogin() }
    }

    private fun makeLogin() {
        if (!Validators.isEmailValid(et_email.text.toString())) {
            showEmailError(true)
        } else {
            val loginData = LoginData(et_email.text.toString(), et_password.text.toString())
            makeLoginAction.onNext(loginData) // EMIT LOGIN ACTION IN LOGIN PRESENTER
        }
    }

    private fun showEmailError(invalid: Boolean) {
        et_email.error = getString(R.string.type_valid_email)
        if (invalid) {
            til_password.error = getString(R.string.invalid_email)
        }
    }

    private fun showPasswordError() {
        et_password.error = getString(R.string.type_valid_password)
        til_password.error = getString(R.string.invalid_credentials)
    }
}
