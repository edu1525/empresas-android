package com.ml.ioasys.features.home.network

import com.ml.ioasys.features.home.infrastructure.model.HomePayload
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface HomeApi {

    @GET("enterprises")
    fun filterCompanies(@Query("name") name: String) : Observable<HomePayload>
}