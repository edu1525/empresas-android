package com.ml.ioasys.features.home.presentation.mapper

import com.ml.ioasys.features.home.domain.model.Company
import com.ml.ioasys.features.home.domain.model.HomeResult
import com.ml.ioasys.features.home.presentation.model.CompanyViewModel
import com.ml.ioasys.features.home.presentation.model.HomeViewModel

class HomeViewModelMapper {
    fun map(homeResult: HomeResult) : HomeViewModel {
        val companies = homeResult.companies.map { mapCompanies(it) }
        return HomeViewModel(companies)
    }

    private fun mapCompanies(company: Company) : CompanyViewModel {
        return CompanyViewModel(
            company.id,
            company.name,
            company.photo,
            company.description,
            company.country
        )
    }
}