package com.ml.ioasys.features.login.presentation.model

data class LoginViewModel(
    val investorName: String?,
    val accessToken: String?,
    val client: String?,
    val uid: String?
)