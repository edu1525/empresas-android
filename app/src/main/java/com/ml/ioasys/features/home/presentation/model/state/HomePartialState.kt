package com.ml.ioasys.features.home.presentation.model.state

import com.ml.ioasys.commom.rx.PartialState
import com.ml.ioasys.features.home.presentation.model.HomeViewModel

sealed class HomePartialState : PartialState{
    object HomeLoading : HomePartialState()
    data class HomeLoaded(val data : HomeViewModel) : HomePartialState()
    data class HomeError(val throwable: Throwable) : HomePartialState()
}