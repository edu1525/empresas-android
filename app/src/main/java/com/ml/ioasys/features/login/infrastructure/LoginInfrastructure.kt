package com.ml.ioasys.features.login.infrastructure

import com.ml.ioasys.features.login.domain.LoginSource
import com.ml.ioasys.features.login.domain.model.LoginResult
import com.ml.ioasys.features.login.infrastructure.mapper.LoginMapper
import com.ml.ioasys.features.login.infrastructure.model.LoginPayload
import com.ml.ioasys.features.login.network.LoginApi
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler

class LoginInfrastructure(
    private val api: LoginApi,
    private val ioScheduler: Scheduler,
    private val loginMapper: LoginMapper
) : LoginSource {

    override fun makeLogin(email: String, password: String): Observable<LoginResult?> {
        return api.makeLogin(LoginPayload(email, password))
            .subscribeOn(ioScheduler)
            .map { loginMapper.map(it) }
    }
}