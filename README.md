
![N|Solid](logo_ioasys.png)

# README #

Projeto desenvolvido em Kotlin/Android Nativo para o teste da vaga de Desenvolvedor Android - IOASYS por **Eduardo de Oliveira Freitas** utilizando a arquitetura **Model View Intent (MVI)**

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbG9A19mazOd4RkcymC1p01qm6mJlaVmGUCxsKUKuQeKpjGWgi)
A MVI é uma arquitetura Reativa, altamente testável, otimizada e de fácil entendimento para desenvolvimento de aplicações de grande porte. Utiliza o conceito de estado da aplicação, através de Intents (Comandos que o usuário executa na tela), dispara ações de entrada que são processadas e retornadas em forma de um novo estado para a tela, de maneira totalmente reativa. Para isso utiliza fortemente o conceito de Observables e  Reducers para geração de novos estados na tela.

Exemplo de Implementação da arquitetura: [Hannes Dorfmann - MODEL-VIEW-INTENT ON ANDROID ](http://hannesdorfmann.com/android/model-view-intent)

### Requisitos ###
 - Android Studio versão 3.6 ou superior
 - SDK da API 29 instalado

### Como Executar ###
Clone o repostório em uma pasta do Sistema Operacional, no Android Studio vá até **File -> Open** , selecione a pasta clonada com o projeto e clique em **OK**. Aguarde a importação pelo Android Studio e após finalizado o processo de sincronização do Graddle e Build, clique no símbolo ▶️ verde na parte superior da tela.

### Bibliotecas Utilizadas ###

 - **Dagger 2:**  Permite a Injeção de Dependências na linguagem Java e Kotlin e também oferece um conjunto de ferramentas para injeção de classes oriundas do framework do Android, permitindo a injeção de dependências em Activities e Fragments.
 
 - **RXJava 3:** A biblioteca permite a coordenação de processamento assíncrono através da utilização de Observables, facilita o gerenciamento de Threads e permite que a aplicação se torne reativa a mudanças de estados.
 
 - **Retrofit 2:** Traz simplicidade na implementação de clientes HTTP para as linguagem Java e Kotlin, Permite realizar chamadas HTTP apenas declarando interfaces anotadas.
 - **Gson + Converter Gson:** Realiza a conversão de objetos Java para JSON e também de JSON para Java. A biblioteca **Converter Gson** permite que a Retrofit consiga converter de forma automática a resposta das chamadas HTTP para objetos do Java.
 - **OKHttp:** Cliente HTTP utilizado pela Retrofit.
 - **RXJava 3 Retrofit Adapter:** Adaptador utilizado pela Retrofit para converter as respostas das chamadas HTTP para Observables.
 - **Glide:** Biblioteca para carregamento e Cache de imagens no Android.

### Pontos a Melhorar ###
Devido ao tempo reduzido do projeto, os seguintes pontos foram deixados para uma implementação futura: 

 - **Handler de erros:** Tratar os erros de forma automática através de um Interceptor os erros retornados pela API após uma requisição na Retrofit.
 
 - **Teste unitário:** Implementação de Testes unitários nas classes: Infrastructure, Mappers, Presenters e ViewModelMappers afim de atingir 85% de cobertura.
 - **Divisão do projeto em Módulos:** Separar as Features e a Injeção de Dependências em diferentes módulos para permitir a melhor manutenção, reaproveitamento de código e Redução do acoplamento.
 - **Automação de CI/CD:** Para permitir a execução de pipelines para automatizar testes no repositório e também na geração do Bundle Assinado.


